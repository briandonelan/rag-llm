import streamlit as st
from langchain_community.llms import VLLM

st.title("🦜🔗 Brian's LLM")

def generate_response(input_text):
    llm = VLLM(model="tiiuae/falcon-7b-instruct",
               max_new_tokens=40,
               trust_remote_code=True,
               download_dir="~/models/",
               temperature=0.5
               )
    st.info(llm(input_text))

with st.form('my_form'):
    text = st.text_area('Enter text:', 'What are the three key pieces of advice for learning how to code?')
    submitted = st.form_submit_button('Submit')
    if submitted:
        generate_response(text)