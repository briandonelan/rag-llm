#!/bin/bash

# Define the service name
SERVICE_NAME="MyPythonApp"

# Create a systemd service file
cat <<EOF > /etc/systemd/system/$SERVICE_NAME.service
[Unit]
Description=Python application service
After=network.target

[Service]
ExecStart=/opt/tensorflow/bin/python -m streamlit run /root/rag-llm/app/main.py --server.port 80
User=$USER
Group=$USER
Restart=always
RestartSec=3
Environment=PYTHONUNBUFFERED=1

[Install]
WantedBy=multi-user.target
EOF

# Reload systemd to recognize the new service
systemctl daemon-reload

# Enable the service to start on boot
systemctl enable $SERVICE_NAME.service

# Start the service
systemctl start $SERVICE_NAME.service

echo "Service $SERVICE_NAME has been installed and started."
