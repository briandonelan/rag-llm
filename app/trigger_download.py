from langchain_community.llms import VLLM
import time

llm = VLLM(model="tiiuae/falcon-7b-instruct",
           trust_remote_code=True,  # mandatory for hf models
           max_new_tokens=128,
           download_dir="~/models/",
           temperature=0.6
)


start_time = time.time()
output = llm("Who is president of US?")
end_time = time.time()
latency = end_time - start_time
print(f"Latency: {latency} seconds")
print("Generated text:", output)